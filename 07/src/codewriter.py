import sys


class CodeWriter:
    # segment memory addresses
    SEGMENTS = {
        'local': "LCL",
        'argument': "ARG",
        'this': "THIS",
        'that': "THAT",
        'pointer': "THIS",
        'temp': 5,
    }

    def __init__(self, output_file):
        self.next_label = 1
        self.input_file = None
        try:
            self.fd = open(output_file, 'w')
        except IOError:
            sys.exit("Cant open file {}".format(output_file))

    def setFileName(self, filename):
        # windows paths?
        self.filename = filename.replace("/", ".")

    def writeArithmetic(self, command):
        if command == "add":
            self._writeln("// add")
            self._write_ab_op("+")
        elif command == "sub":
            self._writeln("// sub")
            self._write_ab_op("-")
        elif command == "neg":
            self._writeln("// neg")
            self._write_unary_op('-')
        elif command == "eq":
            self._writeln("// eq")
            self._write_cmp("JEQ")
        elif command == "gt":
            self._writeln("// gt")
            self._write_cmp("JGT")
        elif command == "lt":
            self._writeln("// lt")
            self._write_cmp("JLT")
        elif command == "and":
            self._writeln("// and")
            self._write_ab_op("&")
        elif command == "or":
            self._writeln("// or")
            self._write_ab_op("|")
        elif command == "not":
            self._writeln("// not")
            self._write_unary_op('!')

    def writePushPop(self, command, segment, index):
        if command == "push":
            self._writeln("// push")
            self._write_push(segment, index)
        elif command == "pop":
            self._writeln("// pop")
            self._write_pop(segment, index)

    # write arithmetic (+, -) or binary (&, |) command
    def _write_ab_op(self, op):
        self._writeln("@SP")
        self._writeln("M=M-1")
        self._writeln("A=M-1")
        self._writeln("D=M")
        self._writeln("A=A+1")
        self._writeln("D=D{}M".format(op))
        self._writeln("A=A-1")
        self._writeln("M=D")

    # write unary (-, |) command
    def _write_unary_op(self, op):
        self._writeln("@SP")
        self._writeln("A=M-1")
        self._writeln("D={}M".format(op))
        self._writeln("M=D")

    def _write_cmp(self, op):
        label = self._get_next_label()
        self._writeln("@SP")
        self._writeln("M=M-1")
        self._writeln("A=M-1")
        self._writeln("D=M")
        self._writeln("A=A+1")
        self._writeln("D=D-M")
        self._writeln("@{}_{}".format(op, label))
        self._writeln("D;{}".format(op))
        self._writeln("@SP")
        self._writeln("A=M-1")
        self._writeln("M=0")
        self._writeln("@{}_{}_END".format(op, label))
        self._writeln("0;JMP")
        self._writeln("({}_{})".format(op, label))
        self._writeln("@SP")
        self._writeln("A=M-1")
        self._writeln("M=-1")
        self._writeln("({}_{}_END)".format(op, label))

    def _write_push(self, segment, index):
        # D=*(segment+index) or
        # D=index if segment=='constant' or
        # D=*(filename.i)
        if segment == 'constant':
            self._writeln("@{}".format(index))
            self._writeln("D=A")
        elif segment == "temp" or segment == "pointer":
            addr = self.SEGMENTS[segment]
            self._writeln("@{}".format(addr))
            self._writeln("D=A")
            self._writeln("@{}".format(index))
            self._writeln("A=D+A")
            self._writeln("D=M")
        elif segment == 'static':
            self._writeln("@{}.{}".format(self.filename, index))
            self._writeln("D=M")
        else:
            addr = self.SEGMENTS[segment]
            self._writeln("@{}".format(addr))
            self._writeln("D=M")
            self._writeln("@{}".format(index))
            self._writeln("A=A+D")
            self._writeln("D=M")

        # *SP=D; SP++
        self._writeln("@SP")
        self._writeln("A=M")
        self._writeln("M=D")
        self._writeln("@SP")
        self._writeln("M=M+1")

    def _write_pop(self, segment, index):
        # R13=*(SP--)
        self._writeln("@SP")
        self._writeln("M=M-1")
        self._writeln("A=M")
        self._writeln("D=M")
        self._writeln("@R13")
        self._writeln("M=D")

        # R14=(segment+index)
        if segment == 'static':
            self._writeln("@{}.{}".format(self.filename, index))
            self._writeln("D=A")
        elif segment == "temp" or segment == "pointer":
            addr = self.SEGMENTS[segment]
            self._writeln("@{}".format(addr))
            self._writeln("D=A")
            self._writeln("@{}".format(index))
            self._writeln("D=D+A")
        else:
            addr = self.SEGMENTS[segment]
            self._writeln("@{}".format(addr))
            self._writeln("D=M")
            self._writeln("@{}".format(index))
            self._writeln("D=D+A")
        self._writeln("@R14")
        self._writeln("M=D")

        # *(segment+index) = *(SP--)
        self._writeln("@R13")
        self._writeln("D=M")
        self._writeln("@R14")
        self._writeln("A=M")
        self._writeln("M=D")

    def _writeln(self, string):
        self.fd.write(string)
        self.fd.write('\n')

    def _get_next_label(self):
        label = self.next_label
        self.next_label += 1
        return label
