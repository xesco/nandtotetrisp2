import sys


class Parser:
    def __init__(self, input_file):
        self.next_command = None
        self.current_command = None
        try:
            self.fd = open(input_file, 'r')
        except IOError:
            sys.exit("Can't open file {}".format(input_file))

    def hasMoreCommands(self):
        if self.next_command:
            return True
        for line in self.fd:
            line = self._clean_line(line)
            if line:
                self.next_command = line
                return True
        return False

    def advance(self):
        if not self.next_command:
            print("Warning: there is no command to parse")
            print("Maybe you need to call Parser.advance()?")
        else:
            self.current_command = self.next_command
            self.next_command = None

    def commandType(self):
        first = self.current_command.split().pop(0)
        if first in ['add', 'sub', 'neg',
                     'eq', 'gt', 'lt',
                     'and', 'or', 'not']:
            return 'C_ARITHMETIC'
        elif first == 'push':
            return 'C_PUSH'
        elif first == 'pop':
            return 'C_POP'
        elif first == 'label':
            return 'C_LABEL'
        elif first == 'goto':
            return 'C_GOTO'
        elif first == 'if-goto':
            return 'C_IF'
        elif first == 'function':
            return 'C_FUNCTION'
        elif first == 'return':
            return 'C_RETURN'
        elif first == 'call':
            return 'C_CALL'
        else:
            sys.exit("Error: wrong VM command", self.current_command)

    def arg1(self):
        ctype = self.commandType()
        command = self.current_command

        if ctype == "C_RETURN":  # add more types here?
            sys.exit("Error: cant apply arg1() to {} command".format(ctype))
        elif ctype == 'C_ARITHMETIC':
            return command.split().pop(0)
        else:
            return command.split().pop(1)

    def arg2(self):
        ctype = self.commandType()
        if ctype in ['C_PUSH', 'C_POP', 'C_FUNCTION', 'C_CALL']:
            return int(self.current_command.split().pop(2))
        else:  # add more types here?
            sys.exit("Error: can't apply arg2() to {} command", ctype)

    def _clean_line(self, line):
        # remove comments
        clean_line = line.split('//').pop(0).strip()
        # remove blanks i.e. extra spaces and tabs
        clean_line = " ".join(clean_line.split())
        # is VM code case sensitive?
        return clean_line.lower()
