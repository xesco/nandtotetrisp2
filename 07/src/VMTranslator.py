#!/usr/bin/env python3

import sys
import argparse

from parser import Parser
from codewriter import CodeWriter


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="Jack VM file")
    args = parser.parse_args()

    name = args.filename.rsplit('.', 1).pop(0)
    ext = args.filename.rsplit('.', 1).pop(-1)
    if ext != "vm":
        sys.exit("Error: extension must be .vm")

    # TODO: check for directory
    parser = Parser(args.filename)
    codewriter = CodeWriter("{}.asm".format(name))
    codewriter.setFileName(name)

    while parser.hasMoreCommands():
        parser.advance()
        ctype = parser.commandType()
        if ctype == 'C_ARITHMETIC':
            command = parser.arg1()
            codewriter.writeArithmetic(command)
        elif ctype == 'C_PUSH':
            arg1 = parser.arg1()
            arg2 = parser.arg2()
            codewriter.writePushPop("push", arg1, arg2)
        elif ctype == 'C_POP':
            arg1 = parser.arg1()
            arg2 = parser.arg2()
            codewriter.writePushPop("pop", arg1, arg2)

        # TODO in chapter 8
        elif ctype == 'C_LABEL':
            pass
        elif ctype == 'C_GOTO':
            pass
        elif ctype == 'C_IF':
            pass
        elif ctype == 'C_FUNCTION':
            pass
        elif ctype == 'C_RETURN':
            pass
        elif ctype == 'C_CALL':
            pass
